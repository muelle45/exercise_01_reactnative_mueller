import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'dart:async';
import 'dart:convert';



Future<List<Album>> fetchAlbum() async {
  final response = await http.get(Uri.parse('https://jsonplaceholder.typicode.com/albums'));

  if (response.statusCode == 200) {
    return parseAlbum(response.body);
  } 
  else {
    throw Exception('Failed to load album');
  }
}



List<Album> parseAlbum(String body) {
  final parsed = jsonDecode(body).cast<Map<String, dynamic>>();

  return parsed.map<Album>((json) => Album.fromJson(json)).toList();
}




class Album {
  final int userId;
  final int id;
  final String title;

  const Album({
    required this.userId,
    required this.id,
    required this.title,
  });


  factory Album.fromJson(Map<String, dynamic> json) {
    return Album(
      userId: json['userId'],
      id: json['id'],
      title: json['title'],
    );
  }
}





void main() => runApp(const MyApp());



class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _MyAppState createState() => _MyAppState();
}




class _MyAppState extends State<MyApp> {
  late Future<List<Album>> futureAlbum;

  @override
  void initState() {
    super.initState();
    futureAlbum = fetchAlbum();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fetch Data and ListViews - Example',
      theme: ThemeData(
        primarySwatch: Colors.red,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text('List of Album'),
        ),
        body: Center(
          child: FutureBuilder<List<Album>>(
            future: futureAlbum,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                return AlbumList(album: snapshot.data!);
              } else if (snapshot.hasError) {
                return Text('${snapshot.error}');
              }

              return const CircularProgressIndicator();
            },
          ),
        ),
      ),
    );
  }
}






class AlbumList extends StatelessWidget{
  const AlbumList({Key? key, required this.album}) : super(key: key);

  final List<Album> album;

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: album.length,
      itemBuilder: (context, int index){
      String title = album[index].title;

          return Center(
            child: Card(
              child: Column(
                children: [
                  ListTile(
                    leading: CircleAvatar(backgroundColor: Colors.blueGrey,
                    child: Text(title[0])),
                    title: Text(album[index].title),
                  ),

                ],
              ),
            ),
          );



        }
    );
  }

}
