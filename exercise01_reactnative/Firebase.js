import { initializeApp } from "firebase/app";
import { getFirestore, collection, getDocs } from 'firebase/firestore';


const firebaseConfig = {
  apiKey: "AIzaSyCNhZTcHSLaIGjP-fzpPrqRTAiWZB9jLos",
  authDomain: "exercise-react-native-433ff.firebaseapp.com",
  projectId: "exercise-react-native-433ff",
  storageBucket: "exercise-react-native-433ff.appspot.com",
  messagingSenderId: "293211634805",
  appId: "1:293211634805:web:99405940c89de56e4ecc3c",
  measurementId: "G-JJPHT7LEPD"
};


const app = initializeApp(firebaseConfig);
  
  export default getFirestore();


  