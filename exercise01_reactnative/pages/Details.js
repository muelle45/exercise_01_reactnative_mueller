import { StyleSheet, Text, View, Button, TextInput, TouchableOpacity, Alert } from 'react-native';
import React, { useState, useEffect } from 'react';
import {collection, doc, onSnapshot, QuerySnapshot, addDoc, Firestore, setDoc, getFirestore, getDoc, docRef, deleteDoc} from "firebase/firestore"



export default function Details({ navigation, route }){

    const [name, onChangeName] = useState(route.params.itemDetails.name);
    const [email, onChangeEmail] = useState(route.params.itemDetails.email);
    const [image, onChangeImage] = useState(route.params.itemDetails.image);


    async function update(){
        await setDoc(doc(getFirestore(), "Users", route.params.itemDetails.id), {
            Name: name,
            Email: email,
            Image: image,
        });
            
        navigation.navigate({
            name: 'UserList',
            merge: true,
        });
    }





    const showAlert = () =>
        Alert.alert(
            "Delete",
            "Do you want to delete the User?",
            [
            {
                text: "Cancel",
                onPress: () => navigation.navigate({name: 'UserList', merge: true,}),
                style: "alertButton",
            },
            {
                text: "Delete",
                onPress: () => deleteUser(),
                style: "alertButton",
            },
            ],
        
        );






    function deleteUser(){
        deleteDoc(doc(getFirestore(), "Users", route.params.itemDetails.id));

        navigation.navigate({
            name: 'UserList',
            merge: true,
        });
    }








    return(
        <View style={styles.container}>
            <TextInput style={styles.input} onChangeText={onChangeName} value={name} multiline = {true} />
            <TextInput style={styles.input} onChangeText={onChangeEmail} value={email} multiline = {true} />
            <TextInput style={styles.input} onChangeText={onChangeImage} value={image} multiline = {true} />

            <TouchableOpacity style={styles.button} onPress={update}>
                <Text style={styles.buttonText}>
                    Update
                </Text>
            </TouchableOpacity>

            <TouchableOpacity style={styles.button} onPress={showAlert}>
                <Text style={styles.buttonText}>
                    Delete
                </Text>
            </TouchableOpacity>
        </View>
    )
    }



const styles = StyleSheet.create({
    container:{
        marginLeft:10,
    },

    input:{
        borderBottomColor:'#CFCDCD',
        borderBottomWidth:1,
        width: 220,
        marginTop: 20,
    },

    button:{
        marginTop:35,
        width: 85,
        height: 35,
        backgroundColor: 'rgba(129, 158, 172, 0.5)',
        borderRadius: 12,
        justifyContent: 'center',
       
    },

    buttonText:{
       height: 28,
       fontStyle: "normal",
       fontWeight: "bold",
       fontSize: 15,
       lineHeight: 22,
       textAlign:'center',
       color: "#5E6A79",
   },

   alertButton:{
        color:'#819EAC',
   }

  });