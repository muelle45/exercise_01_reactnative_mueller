import React, { useState, useEffect } from 'react';
import { StyleSheet, Text, View, Button, SafeAreaView, FlatList,TouchableOpacity, Alert, Image } from 'react-native';
import {collection, doc, onSnapshot, QuerySnapshot, addDoc, Firestore, setDoc, getFirestore, getDocs, docRef} from "firebase/firestore"



export default function UserLists({navigation, route}){

  const [User, setUsers] = useState(null);



  useEffect(()=>{
    
    setUsers(null);
    const colRef = collection(getFirestore(),"Users");
    let user= [];
  

    onSnapshot(colRef, (querySnapshot) => {
      querySnapshot.forEach((doc)=>{
      
        const userData = {name: doc.data().Name, email: doc.data().Email, image: doc.data().Image, id: doc.id};
        user.push(userData);
        setUsers(user);
          
      });
    });
   
  },[])


    


  const Item = ({ item, onPress, backgroundColor, textColor }) => (
    <View>
      <TouchableOpacity onPress={onPress} style={[styles.item, backgroundColor]}>  

      <Image style={styles.tinyLogo} source={{uri: item.image,}}/>
      <Text style={styles.userName}>
        {item.name}  
      </Text>

      </TouchableOpacity>
    </View>
  );





  const [selectedId, setSelectedId] = useState(null);

  const renderItem = ({ item }) => {
    const backgroundColor = item.id === selectedId ? '#6e3b6e' : '#f9c2ff';
    const color = item.id === selectedId ? 'white' : 'black';

    return (
      <Item
        item={item}
        onPress={() => OpenDetails(item)}
        textColor={{ color }}
      />
    );
  };





  const OpenDetails = (item) =>{
  setSelectedId(item.id)
  navigation.navigate('Details',{itemDetails:item}) 
  }





  const EmptyListMessage = () => {
    if(User == null ){  
      Alert.alert("No Data!");
    }
  
    return (
      <View>
        <Text>
          No Data Found 
        </Text>
      </View>
    );
  };
      






    return (
      <SafeAreaView style={styles.container}>

        <TouchableOpacity onPress={()=> {navigation.navigate('CreateNewUser')}}  style={styles.button}>
          <Text style={styles.buttonText}>
            Create new User
          </Text>
        </TouchableOpacity>

        <FlatList
          data={User}  
          renderItem={renderItem}
          keyExtractor={(item, index) => item + index}
          extraData={selectedId}
          ListEmptyComponent={EmptyListMessage}
        />
      </SafeAreaView>
    );

   
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 16,
  },

  tinyLogo: {
    width: 50,
    height: 50,
    borderRadius: 50 / 2,
    overflow: "hidden",
    borderWidth: 3,
    borderColor: "#D9D9D9"
  },

  item: {
    width: 250,
    paddingTop: 20,
    paddingBottom: 20,
    paddingRight: 20,
    marginVertical: 8,
    marginHorizontal: 25,
    borderBottomColor: '#D9D9D9',
    borderBottomWidth: 2,
    flexDirection: "row",
  },

  userName: {
    marginLeft:10,
    marginTop:10,
    width: 180,
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: 16,
    lineHeight: 23,
    color: "#5E6A79",
  },

  button:{
    marginTop:25,
    marginBottom: 20,
    height: 35,
    backgroundColor: 'rgba(129, 158, 172, 0.5)',
    borderRadius: 12,
    justifyContent: 'center',
  },

  buttonText:{
    height: 28,
    margin:10,
    fontStyle: "normal",
    fontWeight: "bold",
    fontSize: 15,
    lineHeight: 22,
    textAlign:'center',
    color: "#5E6A79",
  },

});