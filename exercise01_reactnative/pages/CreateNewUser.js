import { StyleSheet, Text, View, Button, TextInput, TouchableOpacity } from 'react-native';
import React, { useState, useEffect } from 'react';
import {collection, doc, onSnapshot, QuerySnapshot, addDoc, Firestore, setDoc, getFirestore, getDoc, docRef} from "firebase/firestore"



export default function CreateNewUser({ navigation}){

    const [name, onChangeName] = useState('');
    const [email, onChangeEmail] = useState('');
    const [image, onChangeImage] = useState('');



    async function newUser(){
        try{
            await addDoc(collection(getFirestore(), 'Users'),{
                Name: name,
                Email: email,
                Image: image,
            });
        }
        catch(error){
        }
    };





   function Submitt(){
        newUser();

        navigation.navigate({
            name: 'UserList',
            merge: true,
        });
    }







return(
    <View style={styles.container}>
    
        <TextInput style={styles.input} onChangeText={onChangeName} value={name} multiline = {true} placeholder="Name" />
        <TextInput style={styles.input} onChangeText={onChangeEmail} value={email} multiline = {true} placeholder="Email"/>
        <TextInput style={styles.input} onChangeText={onChangeImage} value={image} multiline = {true}  placeholder="Image URL"/>

        <TouchableOpacity style={styles.button} onPress={Submitt}> 
            <Text style={styles.buttonText}>
                Submitt
            </Text>
        </TouchableOpacity>
    
    </View>
)
};

const styles = StyleSheet.create({
    container:{
        marginLeft:10,
    },

    input:{
        borderBottomColor:'#CFCDCD',
        borderBottomWidth:1,
        width: 220,
        marginTop: 20,
    },
     
    button:{
        marginTop:35,
        width: 85,
        height: 35,
        backgroundColor: 'rgba(129, 158, 172, 0.5)',
        borderRadius: 12,
        justifyContent: 'center',   
    },

    buttonText:{
        height: 28,
        fontStyle: "normal",
        fontWeight: "bold",
        fontSize: 15,
        lineHeight: 22,
        textAlign:'center',
        color: "#5E6A79",
    },

    
  });
  