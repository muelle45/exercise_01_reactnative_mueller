import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import  UserList from './pages/UserList'
import  Details from './pages/Details'
import  CreateNewUser from './pages/CreateNewUser'

export default function App() {

  const Stack = createNativeStackNavigator();
  

  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="UserList" component={UserList} options={{ title: 'User List' }}/>
        <Stack.Screen name="Details" component={Details} options={{ title: 'Details' }}/>
        <Stack.Screen name="CreateNewUser" component={CreateNewUser} options={{ title: 'Create a New User' }}/>
      </Stack.Navigator>
    </NavigationContainer>
  );

}









